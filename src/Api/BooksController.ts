import { Body, Controller, Post, Route, SuccessResponse } from "tsoa";

import { StatusCodes } from "http-status-codes";
import BookRepository from "../Infrastructure/Data/Repositories/BookRepository";
import { InsertBookInLibraryCommand } from "../Core/Domain/Book/InsertBookInLibraryCommand";

@Route("api/v1/books")
export class BookController extends Controller {
  @Post("addBook")
  @SuccessResponse("201", "Created")
  public addBook(@Body() command: InsertBookInLibraryCommand): Promise<string> {
    this.setStatus(StatusCodes.CREATED);
    return BookRepository.AddAsync(command);
  }
}