import { Sequelize } from "sequelize";
import bookKeywords from "../../Core/AggregatesModel/BookKeywords";
import books from "../../Core/AggregatesModel/Books";
import keywords from "../../Core/AggregatesModel/Keywords";

const getDbContext = () => new Sequelize({
  host: process.env.PGHOST ?? "",
  username: process.env.PGUSER ?? "",
  password: process.env.PGPASSWORD ?? "",
  database: process.env.PGDATABASE ?? "",
  port: parseInt(process.env.PGPORT ?? "5432"),
  dialect: "postgres",
  dialectOptions: {},
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

const sequelize = getDbContext();
const booksModel = books(sequelize);
const keywordsModel = keywords(sequelize);
bookKeywords(sequelize);

keywordsModel.belongsToMany(booksModel, {
  through: "bookKeywords",
  as: "Books",
  foreignKey: "KeywordId",
});

booksModel.belongsToMany(keywordsModel, {
  through: "bookKeywords",
  as: "Keywords",
  foreignKey: "BookId",
});

sequelize.sync();

export default getDbContext;