import books, { Book } from "../../../Core/AggregatesModel/Books";
import bookKeywords from "../../../Core/AggregatesModel/BookKeywords";
import keywords from "../../../Core/AggregatesModel/Keywords";
import { InsertBookInLibraryCommand } from "../../../Core/Domain/Book/InsertBookInLibraryCommand";
import { DomainOfAggregate } from "../../../Core/SeedWork/DomainOfAggregate";
import getContext from "../BookLibraryContext";

export default {
  async AddAsync(command: InsertBookInLibraryCommand): Promise<string> {
    const childEntities = await Promise.all(command.Keywords.map(async r => {
      return keywords(getContext()).create({
        Name: r.Name,
        Description: r.Description
      });
    }));
    
    const entity = await books(getContext()).create({
      Author: command.Author,
      Genre: command.Genre,
      Name: command.Name,
      MaximumDaysForRental: command.MaximumDaysForRental,
      Keywords: childEntities
    });
    await Promise.all(childEntities.map(r => {
      return bookKeywords(getContext()).create({ KeywordId: r.Id, BookId: entity.Id });
    }));

    return entity.Id;
  },
  GetAsync(_aggregateId: number): Promise<DomainOfAggregate<Book>> {
    throw new Error("Method not implemented.");
  },
  SaveAsync(): Promise<void> {
    throw new Error("Method not implemented.");
  }
};