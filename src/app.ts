import express from "express";
import bodyParser from "body-parser";
import { RegisterRoutes } from "../build/routes";
import swaggerDoc from "../build/swagger.json";
import * as swaggerUI from "swagger-ui-express";

export const app = express();

// Use body parser to read sent json payloads
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.use('/swagger', swaggerUI.serve, swaggerUI.setup(swaggerDoc));

RegisterRoutes(app);