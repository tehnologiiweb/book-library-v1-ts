import { ICreateAggregateCommand } from "../../SeedWork/ICreateAggregateCommand";

export interface InsertKeyworkCommand {
  Name: string;
  Description: string;
}

export interface InsertBookInLibraryCommand extends ICreateAggregateCommand {
  Name: string;
  Author: string;
  Genre: string;
  MaximumDaysForRental: number;
  Keywords: InsertKeyworkCommand[];
}
