import { Book } from "../../AggregatesModel/Books";
import { Keyword } from "../../AggregatesModel/Keywords";
import { DomainOfAggregate } from "../../SeedWork/DomainOfAggregate";

export class BookDomain extends DomainOfAggregate<Book> {
  constructor(aggregate: Book) {
    super(aggregate);
  }

  UpdateBook(name: string, author: string, genre: string, maximumDaysForRental: number, keywords: Keyword[]) {
    this.aggregate.Name = name;
    this.aggregate.Author = author;
    this.aggregate.Genre = genre;
    this.aggregate.MaximumDaysForRental = maximumDaysForRental;
    this.aggregate.Keywords = keywords;
  }

  Delete() {
    this.aggregate.CanBeDeleted = !this.aggregate.IsBooked;

    return this.aggregate.CanBeDeleted;
  }

  IsBookAvailableForRental() {
    return !this.aggregate.IsBooked;
  }

  GetMaximumDaysForRental() {
    return this.aggregate.MaximumDaysForRental;
  }

  MarkBookAsRented() {
    this.aggregate.IsBooked = true;
  }

  MarkBookAsAvailable() {
    this.aggregate.IsBooked = false;
  }
}