import { Book } from "../../AggregatesModel/Books";
import { IRepositoryOfAggregate } from "../../SeedWork/IRepositoryOfAggregate";
import { InsertBookInLibraryCommand } from "./InsertBookInLibraryCommand";

export interface IBookRepository extends IRepositoryOfAggregate<Book, InsertBookInLibraryCommand> {}