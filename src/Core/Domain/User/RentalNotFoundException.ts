export class RentalNoFoundException extends Error {
  constructor(bookId: number) {
    super(`Rental for book ${bookId} either finished or not found!`);
  }
}