export class UserReturnedBookEvent {
  readonly BookId: number;

  constructor(bookId: number) {
    this.BookId = bookId;
  }
}