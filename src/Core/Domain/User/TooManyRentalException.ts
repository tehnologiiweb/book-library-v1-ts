export class TooManyRentalExceptions extends Error {
  constructor() {
    super("Current user has too many rentals at this moment!");
  }
}