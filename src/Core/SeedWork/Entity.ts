import { Model } from "sequelize";

export abstract class Entity extends Model {
  Id: string = null!;
}