import { DomainOfAggregate } from "./DomainOfAggregate";
import { Entity } from "./Entity";
import { ICreateAggregateCommand } from "./ICreateAggregateCommand";
import { IAggregateRoot } from "./IAggregateRoot";

abstract class EntityAndIAggregateRoot extends Entity implements IAggregateRoot {}
export interface IRepositoryOfAggregate<TAggregate extends EntityAndIAggregateRoot, TAggregateCommand extends ICreateAggregateCommand> {
  AddAsync(command: TAggregateCommand): Promise<string>
  GetAsync(aggregateId: number): Promise<DomainOfAggregate<TAggregate>>
  SaveAsync(): Promise<void>
}