import { Entity } from "./Entity";
import { IAggregateRoot } from "./IAggregateRoot";

abstract class EntityAndIAggregateRoot extends Entity implements IAggregateRoot {}
export abstract class DomainOfAggregate<TAggregate extends EntityAndIAggregateRoot> {
  readonly aggregate: TAggregate;
  
  constructor(aggregate: TAggregate) {
    this.aggregate = aggregate;
  }
}