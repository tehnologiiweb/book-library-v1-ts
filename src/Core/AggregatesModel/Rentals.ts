import { Entity } from "../SeedWork/Entity";
import { Users } from "./Users";

export class Rentals extends Entity {
  BookId: number = null!;
  ReaderId: number = null!;
  RentalStartDate: Date = null!;
  ExpectedRentalEndDate: Date = null!;
  ActualRentalEndDate?: Date = null!;
  Reader?: Users;
}