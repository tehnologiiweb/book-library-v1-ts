import { Sequelize, TEXT, UUID, UUIDV4 } from "sequelize";
import { Entity } from "../SeedWork/Entity";

export class Keyword extends Entity {
  Name: string = null!;
  Description: string = null!;
}

export default (sequelize: Sequelize) => {
  return sequelize.define<Keyword>("keywords", {
    Id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    Name: {
      type: TEXT,
      allowNull: false
    },
    Description: {
      type: TEXT,
      allowNull: false
    }
  }, {
    timestamps: true
  });
};