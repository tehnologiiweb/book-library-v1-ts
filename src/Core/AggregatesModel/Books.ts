import { Entity } from "../SeedWork/Entity";
import { IAggregateRoot } from "../SeedWork/IAggregateRoot";
import { Keyword } from "./Keywords";
import { Sequelize, UUID, TEXT, UUIDV4, INTEGER, BOOLEAN } from "sequelize";

export class Book extends Entity implements IAggregateRoot {
  Name: string = null!;
  Author: string = null!;
  Genre: string = null!;
  MaximumDaysForRental: number = null!;
  Keywords: Keyword[] = [];
  CanBeDeleted = false;
  IsBooked = false;
}

export default (sequelize: Sequelize) => {
  return sequelize.define<Book>("books", {
    Id: {
      type: UUID,
      primaryKey: true,
      defaultValue: UUIDV4
    },
    Name: {
      type: TEXT,
      allowNull: false
    },
    Author: {
      type: TEXT,
      allowNull: false
    },
    Genre: {
      type: TEXT,
      allowNull: false
    },
    MaximumDaysForRental: {
      type: INTEGER,
      allowNull: false
    },
    CanBeDeleted: {
      type: BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    IsBooked: {
      type: BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: true
  });
};
