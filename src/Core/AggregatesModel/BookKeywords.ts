import { Sequelize, UUID, Model } from "sequelize";

export class BookKeyword extends Model {
  BookId: string = null!;
  KeywordId: string = null!;
}

export default (sequelize: Sequelize) => {
  return sequelize.define<BookKeyword>("bookKeywords", {
    BookId: {
      type: UUID,
      primaryKey: true,
      allowNull: false
    },
    KeywordId: {
      type: UUID,
      primaryKey: true,
      allowNull: false
    }
  }, {
    timestamps: true
  });
};
