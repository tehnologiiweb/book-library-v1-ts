import { Entity } from "../SeedWork/Entity";
import { IAggregateRoot } from "../SeedWork/IAggregateRoot";
import { Rentals } from "./Rentals";

export class Users extends Entity implements IAggregateRoot {
  IdentityId?: string;
  Email?: string;
  Name?: string;
  PhoneNumber?: string;
  Address?: string;
  Rentals: Rentals[] = [];
}